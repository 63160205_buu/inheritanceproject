/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.inheritance;

/**
 *
 * @author acer
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal ("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        
        Dog To = new Dog("To","orange");
        To.speak();
        To.walk();
        
        Dog Mome = new Dog("Mome","Black&White");
        Mome.speak();
        Mome.walk();
        
        Dog Bat = new Dog("Bat","Black&White");
        Bat.speak();
        Bat.walk();
        
        Cat zero = new Cat("Zero", "orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom", "orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck GabGab = new Duck("GabGab", "black");
        GabGab.speak();
        GabGab.walk();
        GabGab.fly();
        
        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Object: " + (zom instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom ;
        ani2 = zero ;
        
        System.out.println("Ani1: zom is Duck " + (ani1 instanceof Duck));
        
        
        Animal[] animals = {dang,To,Mome,Bat,zero,zom,GabGab};
        for(int i = 0 ; i< animals.length ;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
                
            }
        }
    }
}
