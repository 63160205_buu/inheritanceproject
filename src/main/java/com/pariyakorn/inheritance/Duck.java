/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.inheritance;

/**
 *
 * @author acer
 */
public class Duck extends Animal{
    private int numberOfwings;
     public Duck(String name, String color){
        super(name, color,2);
        
     }
     public void fly(){
         System.out.println("Duck: " + name + " fly!!!");
     }
     @Override
      public void walk(){
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs. ");
    }
     @Override
      public void speak () {
         System.out.println("Duck: " + name + " speak > quack quack !!");
      }
     
}
